<?php

namespace App\Http\Controllers;

use Alaouy\Youtube\Facades\Youtube;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
   public function getVideoInfo($video_id)
   {
   	$video = Youtube::getVideoInfo($video_id);

	   if (!$video) {
		   return 'Record not found';
	   }

	   return json_decode(json_encode($video), true);
   }

	public function getPopularVideos($country)
	{
		$video = Youtube::getPopularVideos($country);

		if (!$video) {
			return 'Record not found';
		}

		return json_decode(json_encode($video), true);
	}

	public function getChannelInfoByName($name)
	{
		$channel = Youtube::getChannelByName($name);

		if (!$channel) {
			return 'Record not found';
		}

		return json_decode(json_encode($channel), true);
	}

	public function getChannelInfoById($channel_id)
	{
		$channel = Youtube::getChannelById($channel_id);

		if (!$channel) {
			return 'Record not found';
		}

		return json_decode(json_encode($channel), true);
	}

	public function getPlaylistInfoById($list_id)
	{
		try {
			$video = Youtube::getPlaylistById($list_id);
			if (!$video) {
				return 'Record not found';
			}

			return json_decode(json_encode($video), true);
		}
		catch (\Exception $e) {
			return 'Server Error';
		}

	}

	public function getPlaylistsByChannelId($channel_id)
	{
		$video = Youtube::getPlaylistsByChannelId($channel_id);
		if (!$video) {
			return 'Record not found';
		}

		return json_decode(json_encode($video), true);
	}

	public function getActivitiesByChannelId($channel_id)
	{
		$video = Youtube::getActivitiesByChannelId($channel_id);
		if (!$video) {
			return 'Record not found';
		}

		return json_decode(json_encode($video), true);
	}

	public function parseVidFromUrl(Request $request)
	{
		$url = $request->get('url', false);

		if ($url)
			$video = Youtube::parseVIdFromURL($url);
		else
			return 'Missing parameter "url"';
		if (!$video) {
			return 'Record not found';
		}


		return [ 'vid' => $video ];
	}

	public function search(Request $request)
	{

		$search = $request->get('q', false);
		if ($search === false)
			return 'Missing parameter "q".';

		//Parameters
		$params        = array(
			'q'          => $search,
			'type'       => 'video',
			'part'       => 'id, snippet',
			'maxResults' => 50
		);

		// store page tokens for next and back
		$pageTokens = array();

		//inital search
		$search = Youtube::paginateResults($params, null);

		// Store token
		$pageTokens[] = $search['info']['nextPageToken'];

		// Go to next page in result
		$search = Youtube::paginateResults($params, $pageTokens[0]);

		// Store token
		$pageTokens[] = $search['info']['nextPageToken'];

		// Go to next page in result
		$search = Youtube::paginateResults($params, $pageTokens[1]);

		// Store token
		$pageTokens[] = $search['info']['nextPageToken'];

		// Go back a page
		$search = Youtube::paginateResults($params, $pageTokens[0]);

		return json_decode(json_encode($search), true);

	}
}
