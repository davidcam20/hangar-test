<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SongsController extends Controller
{

	private $rules;

	public function __construct()
	{
		$this->rules = $rules = [
			'name' => 'required',
			'url' => 'required',
			'album_id' => 'required',
			'artist_id' => 'required'
		];
	}


    public function createSong(Request $request)
	{
		// validate if request is an array
		if (!is_array($request->all())) {
			return ['error' => 'request must be an array'];
		}

		//check if the data if complete
			$validator = Validator::make($request->all(),$this->rules);
			if ($validator->fails()) {
				return [
					'created' => false,
					'errors'  => $validator->errors()->all()
				];
			}

			//save the new song
			$song = new Song($request->all());
			$song->save();

			$data = array(
				'created' => true,
				'song' => $song
			);

			return $data;
	}

	public function updateSong(Request $request, $id)
	{
		// validate if request is an array
		if (!is_array($request->all())) {
			return ['error' => 'request must be an array'];
		}

		$song = Song::findOrfail($id);

		//check if the data if complete
		$validator = Validator::make($request->all(),$this->rules);
		if ($validator->fails()) {
			return [
				'created' => false,
				'errors'  => $validator->errors()->all()
			];
		}

		//update the song
		$song->update($request->all());

		$data = array(
			'update' => true,
			'song' => $song
		);

		return $data;
	}

	public function getInfoSong($id)
	{
		$song =  Song::findOrFail($id);

		$data = array(
			'id' => $song->id,
			'name' => $song->name,
			'album_id' => $song->album_id,
			'album_name' => $song->album->name,
			'artist_id' => $song->artist_id,
			'artist_name' => $song->artist->name,
			'url' => $song->url,
		);

		return $data;
	}

	public function deleteSong($id)
	{
		$song = Song::findOrfail($id);
		$song->delete();

		$data = array(
			'delete' => true,
		);

		return $data;
	}

	public function searchSongs(Request $request)
	{
		if($request->has('keyword')) {
			$keyword = $request->get('keyword');
			$songs = Song::searchSongs($keyword)->get();

			$data = array();

			foreach ($songs as $song) {
				$data[] = array(
					'id' => $song->id,
					'name' => $song->name,
					'album_id' => $song->album_id,
					'album_name' => $song->album->name,
					'artist_id' => $song->artist_id,
					'artist_name' => $song->artist->name,
					'url' => $song->url
					);
			}

			return $data;
		}else{
			return 'Model not found';
		}
	}
}
