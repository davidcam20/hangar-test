<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Song extends Model
{

	protected $table = 'songs';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'url', 'artist_id', 'album_id'];

	public function artist_id(){
		return $this->belongsTo(Artist::class);
	}

	public function album_id(){
		return $this->belongsTo(Album::class);
	}

	public function artist(){
		return $this->belongsTo(Artist::class);
	}

	public function album(){
		return $this->belongsTo(Album::class);
	}

	public static function searchSongs($keyword)
	{
		if ($keyword!='') {
			$query = Song::select(DB::raw('songs.*'))
				->join('artists','artists.id','=','songs.artist_id')
				->join('albums','albums.id','=','songs.album_id');
			$query->where(function ($query) use ($keyword) {
				$query->where("songs.name", "LIKE","%$keyword%")
					->orWhere("artists.name", "LIKE", "%$keyword%")
					->orWhere("albums.name", "LIKE", "%$keyword%");
			});
		}
		return $query;
	}
}
