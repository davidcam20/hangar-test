LARAVEL - API SONGS
=============

**Autor:**


David Camacho Granados - Web Developer

**Valid Content-Type:**

 * application/x-www-form-urlencoded
 * multipart/form-data
 * application/json

**CRUD SONGS Actions:**

 METHOD      | PATH                | ACTION        | NAME
 ----------- | ------------------- | ------------- | -------------
 GET         | /api/songs/v1/search     | search        | search
 GET         | /api/songs/v1/{id}       | search_by_id  | get_by_id
 POST        | /api/songs/v1            | create        | create
 PUT         | /api/songs/v1/{id}       | update        | update
 DELETE      | /api/songs/v1/{id}       | delete        | delete

 **Song Attributes: **

 Attribute     | Type          | Required      | Generated
 ------------- | ------------- | ------------- | -------------
 id            | integer       | No            | Yes
 name          | string        | Yes           | No
 url           | string        | Yes           | No
 artist_id     | integer       | Yes           | No
 album_id      | integer       | Yes           | No
 created_at    | datetime      | No            | Yes
 updated_at    | datetime      | No            | Yes

**Examples**

**URL: api/songs/v1/1**
**METHOD: GET**
**DATA:**

    null

 **RESULT**

     {
     id: 1,
     name: "Symphony of destruction",
     album_id: 1,
     album_name: "Countdown to extinction",
     artist_id: 1,
     artist_name: "Megadeth",
     url: "https://www.youtube.com/watch?v=K5jvUXij7nU"
     }

 **URL: api/songs/v1**
 **METHOD: POST**
 **DATA:**

     songname   :Symphony of destruction
     artist_id   :1
     album_id    :1
     url: "https://www.youtube.com/watch?v=K5jvUXij7nU"

 **RESULT**

     {
       "created":true
       "song":
         {
            id: 1,
            name: "Symphony of destruction",
            artist_id: 1,
            album_id: 1,
            url: "https://www.youtube.com/watch?v=K5jvUXij7nU"
            "created_at": "2017-01-19 06:27:37",
            "updated_at": "2017-01-10 06:27:37"
         }
     }

 **URL: api/songs/v1/search**
 **METHOD: GET**
 **DATA:**

     keyword :destruction

 **RESULT**

     [
       {
          id: 1,
          name: "Symphony of destruction",
          album_id: 1,
          album_name: "Countdown to extinction",
          artist_id: 1,
          artist_name: "Megadeth",
          url: "https://www.youtube.com/watch?v=K5jvUXij7nU"
       }
     ]

 **URL: api/songs/v1/1**
 **METHOD: PUT**
 **DATA:**

     songname   :Symphony of destruction - Megadeth
     artist_id  :1
     album_id   :1
     url: "https://www.youtube.com/watch?v=K5jvUXij7nU"

 **RESULT**

     {
        "update":true
        "song":
            {
              id: 1,
              name: "Symphony of destruction - Megadeth",
              artist_id: 1,
              album_id: 1,
              url: "https://www.youtube.com/watch?v=K5jvUXij7nU"
              "created_at": "2017-01-19 06:27:37",
              "updated_at": "2017-01-10 06:27:37"
            }
     }

 **URL: api/songs/v1/1**
 **METHOD: DELETE**
 **DATA:**

    Null

 **RESULT**

     {"delete":true}


 **This project have voyager admin**

 User: admin@admin.com
 Password: password

 The database is in the project.

Voyager is in use because I created a admin of artist,albums and songs and I relate the song with the artist and the album.

 API Youtube.

 I use alaouy/youtube for to connect to api fo Youtube and create a api for create a little bridge.

 All methods are GET.

URL                                        | ACTION                       | PARAM GET
-------------------------------------------|------------------------------|---------------------
"api/youtube/v1/info/{vid}"                | getVideoInfo                 | vid
"api/youtube/v1/channel/{name}"            | getChannelInfoByName         | name
"api/youtube/v1/popular/{country}"         | getPopularVideos             | country
"api/youtube/v1/channel/id/{vid}"          | getChannelInfoById           | vid
"api/youtube/v1/playlist/{vid}"            | getPlaylistInfoById          | vid
"api/youtube/v1/playlist/channel/{vid}"    | getPlaylistsByChannelId      | vid
"api/youtube/v1/activities/channel/{vid}"  | getActivitiesByChannelId     | vid
"api/youtube/v1/video/parse"               | parseVidFromUrl              | url (query)
"api/youtube/v1/search"                    | search                       | q, maxResults (optional) , prevPageToken (optional), nextPageToken (optional). All query


**URL: api/youtube/v1/info/K5jvUXij7nU**
 **METHOD: GET**
 **DATA:**

     null

 **RESULT**

     {
        kind: "youtube#video",
        etag: ""gMxXHe-zinKdE9lTnzKu8vjcmDI/yV8_ECrwMAa_9X5Zj6VNG57qk_4"",
        id: "K5jvUXij7nU",
        snippet: {
            publishedAt: "2008-11-11T02:25:52.000Z",
            channelId: "UC-04y9GjnbvK4UIotvZUR3Q",
            title: "Megadeth - Symphony of Destruction (Studio Version)",
            description: "Symphony of Destruction is the second song on the thrash metal band Megadeths 5th studio album, Countdown to Extinction. The song is one of the heaviest songs written by the band. While it isnt known for its speed, its definately famed for its heaviness. The songs lyrical meaning depicts giving someone power and watching them grow. You take a mortal man, And put him in control Watch him become a god, Watch peoples heads aroll Aroll... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction Acting like a robot, Its metal brain corrodes. You try to take its pulse, Before the head explodes. Explodes... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction The earth starts to rumble World powers fall Awarring for the heavens, A peaceful man stands tall Tall... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction",
        thumbnails: {
            default: {
                url: "https://i.ytimg.com/vi/K5jvUXij7nU/default.jpg",
                width: 120,
                height: 90
            },
            medium: {
                url: "https://i.ytimg.com/vi/K5jvUXij7nU/mqdefault.jpg",
                width: 320,
                height: 180
            },
            high: {
                url: "https://i.ytimg.com/vi/K5jvUXij7nU/hqdefault.jpg",
                width: 480,
                height: 360
            }
        },
        channelTitle: "gibsonscottyozzy",
        tags: [
            "megadeth",
            "countdown",
            "to",
            "extinction",
            "symphony",
            "of",
            "destruction",
            "dave",
            "mustaine"
        ],
        categoryId: "10",
        liveBroadcastContent: "none",
        localized: {
            title: "Megadeth - Symphony of Destruction (Studio Version)",
            description: "Symphony of Destruction is the second song on the thrash metal band Megadeths 5th studio album, Countdown to Extinction. The song is one of the heaviest songs written by the band. While it isnt known for its speed, its definately famed for its heaviness. The songs lyrical meaning depicts giving someone power and watching them grow. You take a mortal man, And put him in control Watch him become a god, Watch peoples heads aroll Aroll... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction Acting like a robot, Its metal brain corrodes. You try to take its pulse, Before the head explodes. Explodes... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction The earth starts to rumble World powers fall Awarring for the heavens, A peaceful man stands tall Tall... Just like the pied piper Lead rats through the streets We dance like marionettes, Swaying to the symphony... Of destruction"
        }
     },
     contentDetails: {
        duration: "PT4M4S",
        dimension: "2d",
        definition: "sd",
        caption: "false",
        licensedContent: false,
        projection: "rectangular"
     },
     status: {
        uploadStatus: "processed",
        privacyStatus: "public",
        license: "youtube",
        embeddable: true,
        publicStatsViewable: true
     },
     statistics: {
        viewCount: "8549235",
        likeCount: "38449",
        dislikeCount: "933",
        favoriteCount: "0",
        commentCount: "3122"
     },
     player: {
        embedHtml: "<iframe width="480" height="360" src="//www.youtube.com/embed/K5jvUXij7nU" frameborder="0" allowfullscreen></iframe>"
        }
     }