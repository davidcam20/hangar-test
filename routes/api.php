<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::group([ 'prefix' => 'songs/v1' ], function () {

	Route::get('/search', 'SongsController@searchSongs')
		->name('search');

	Route::get('/{id}', 'SongsController@getInfoSong')
		->name('get_by_id');

	Route::post('/', 'SongsController@createSong')
		->name('create');

	Route::put('/{id}', 'SongsController@updateSong')
		->name('update');

	Route::delete('/{id}', 'SongsController@deleteSong')
		->name('delete');
});

Route::group([ 'prefix' => 'youtube/v1' ], function () {
	Route::get('/info/{vid}', 'YouTubeController@getVideoInfo')
		->name('youtube_video_info');

	Route::get('/channel/{name}', 'YouTubeController@getChannelInfoByName')
		->name('youtube_info_channel_name');

	Route::get('/popular/{country}', 'YouTubeController@getPopularVideos')
		->name('youtube_popular_videos');

	Route::get('/channel/id/{vid}', 'YouTubeController@getChannelInfoById')
		->name('youtube_info_channel_id');

	Route::get('/playlist/{vid}', 'YouTubeController@getPlaylistInfoById')
		->name('youtube_info_playlist');

	Route::get('/playlist/channel/{vid}', 'YouTubeController@getPlaylistsByChannelId')
		->name('youtube_info_channel_playlist');

	Route::get('/activities/channel/{vid}', 'YouTubeController@getActivitiesByChannelId')
		->name('youtube_activities_channel');

	Route::get('/video/parse', 'YouTubeController@parseVidFromUrl')
		->name('youtube_info_parse_video');

	Route::get('/search', 'YouTubeController@search')
		->name('youtube_search');

});
